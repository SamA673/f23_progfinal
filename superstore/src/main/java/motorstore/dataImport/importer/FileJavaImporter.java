package motorstore.dataImport.importer;

import java.nio.file.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import motorstore.datalogic.product.*;

/**
 * This class represents a Java implementation of a vehicle importer that reads
 * and writes data from/to files.
 * It implements the IVehicleImporter interface and provides methods to load and
 * save vehicles, as well as
 * managing the loyalty system.
 *
 * @author Sam A
 * 
 */

public class FileJavaImporter implements IVehicleImporter {
    private String fileName;
    private String loyaltyFileName;

    /**
     * Constructs a FileJavaImporter with the specified file names (the relative
     * path) for vehicle and loyalty data.
     *
     * @param fileName        the name of the file containing vehicle data
     * @param loyaltyFileName the name of the file containing loyalty program data
     */
    public FileJavaImporter(String fileName, String loyaltyFileName) {
        this.fileName = fileName;
        this.loyaltyFileName = loyaltyFileName;
    }

    /**
     * Constructs a FileJavaImporter with the specified file name for vehicle data.
     * Loyalty program data is not considered in this case.
     *
     * @param fileName the name of the file containing vehicle data
     */
    public FileJavaImporter(String fileName) {
        this.fileName = fileName;
        this.loyaltyFileName = ""; // In case we don't care about loyalty.
    }

    /**
     * Gets the name of the file containing vehicle data.
     *
     * @return the file name
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * Loads a list of vehicles from the specified file.
     *
     * @return a list of vehicles
     * @throws ImportDataException if an error occurs during the data import process
     */
    @Override
    public List<Vehicle> loadVehicles() throws ImportDataException {
        try {
            Path p = Paths.get(fileName);
            List<String> allLines = Files.readAllLines(p);
            List<Vehicle> vehicles = new ArrayList<Vehicle>();
            for (String line : allLines) {
                String[] fields = line.split(",");

                if (fields.length != 10) {
                    throw new IllegalArgumentException("Vehicles require 10 inputs");
                }

                if (fields[0].equals("Plane")) {
                    vehicles.add(new Plane(fields));

                } else if (fields[0].equals("Car")) {
                    vehicles.add(new Car(fields));
                }

            }
            return vehicles;
        } catch (IOException e) {
            throw new ImportDataException(e);
        }
    }

    /**
     * Saves a list of vehicles to the specified file.
     *
     * @param vehicles the list of vehicles to be saved
     * @throws ExportDataException if an error occurs during the data export process
     */
    @Override
    public void saveVehicles(List<Vehicle> vehicles) throws ExportDataException {
        Path p = Paths.get(this.fileName);
        List<String> allLines = new ArrayList<String>();

        for (Vehicle v : vehicles) {
            allLines.add(v.export());
        }

        try {
            Files.write(p, allLines);
        } catch (IOException e) {
            throw new ExportDataException(e.getCause());
        }
    }

    /**
     * Saves loyalty program data to the specified file.
     *
     * @param loyaltyMembers a map containing customer IDs as keys and lists of
     *                       associated brands as values
     * @throws ExportDataException if an error occurs during the data export process
     */
    public void saveLoyalty(Map<String, List<String>> loyaltyMembers) throws ExportDataException {
        Path p = Paths.get(this.loyaltyFileName);
        List<String> allLines = new ArrayList<String>();

        for (String key : loyaltyMembers.keySet()) {
            String line = key + ",";
            for (String brand : loyaltyMembers.get(key)) {
                line += brand + ",";
            }
            allLines.add(line);
        }

        try {
            Files.write(p, allLines);
        } catch (IOException e) {
            throw new ExportDataException("<ERROR> A problem occured trying to save the loyalty program locally.");
        }
    }

    /**
     * Loads loyalty program data from the specified file.
     *
     * @return a map containing customer IDs as keys and lists of associated brands
     *         as values
     * @throws ImportDataException if an error occurs during the data import process
     */
    public Map<String, List<String>> loadLoyalty() throws ImportDataException {
        try {
            Path p = Paths.get(this.loyaltyFileName);
            List<String> allLines = Files.readAllLines(p);
            Map<String, List<String>> customers = new HashMap<String, List<String>>();

            for (String line : allLines) {
                String[] customer = line.split(",");
                customers.put(customer[0], new ArrayList<String>());
                for (int i = 1; i < customer.length; i++) {
                    customers.get(customer[0]).add(customer[i]);
                }
            }
            return customers;
        } catch (IOException e) {
            throw new ImportDataException(e);
        }
    }
}
