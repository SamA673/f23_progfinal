package motorstore.dataImport.importer;

import java.util.ArrayList;
import java.util.List;
import motorstore.datalogic.product.*;
import motorstore.utilities.Input;
import oracle.jdbc.OracleTypes;
import java.sql.*;

/**
 * The SqlVehicleImporter class implements the IVehicleImporter interface and
 * provides functionality to load and save vehicle data to or from an Oracle SQL
 * database (USING JDBC)
 * 
 * @author David P
 */
public class SqlVehicleImporter implements IVehicleImporter {
    private Connection conn;
    private final String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";

    /**
     * Constructs a SqlVehicleImporter and creates a connection to the Oracle SQL
     * database.
     * The connection details are specified in the class URL constant.
     */
    public SqlVehicleImporter() {
        try {
            this.conn = createConnection();
        } catch (SQLException e) {
            System.out.println("There was an error creating a connection");
        }
    }

    /**
     * Loads a list of vehicles from the 'Vehicles' table in database.
     *
     * @return a list of vehicles
     * @throws ImportDataException if an error occurs during the data import process
     */
    @Override
    public List<Vehicle> loadVehicles() throws ImportDataException {
        try (CallableStatement callableStatement = this.conn.prepareCall("{call motorstore_package.load_vehicles(?,?)}")) {
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.registerOutParameter(2, OracleTypes.CURSOR);
            callableStatement.execute();
            ResultSet vehicleCursor = (ResultSet) callableStatement.getObject(1);
            ResultSet typeCursor = (ResultSet) callableStatement.getObject(2);

            ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

            while (vehicleCursor.next() && typeCursor.next()) {
                vehicles.add(createVehicleFromResult(vehicleCursor, typeCursor));
            }
            return vehicles;
        } catch (SQLException e) {
            throw new ImportDataException(e.getCause());
        }
    }

    /**
     * Saves a list of vehicles to the 'Vehicles' table in the Oracle SQL database.
     * Initially, it will delete all existing records in the table, and then it
     * inserts the new records.
     *
     * @param vehicles the list of vehicles to be saved
     * @throws ExportDataException if an error occurs during the data export process
     */
    @Override
    public void saveVehicles(List<Vehicle> vehicles) throws ExportDataException {
        try (Statement stmt = this.conn.createStatement()) {
            String sql = "DELETE FROM Vehicles";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            throw new ExportDataException(e.getCause());
        }
        for (Vehicle vehicle : vehicles) {
            try {
                if (vehicle instanceof Vehicle) {
                    vehicle.addToDatabase(this.conn);
                }
            } catch (SQLException | ClassNotFoundException e) {
                throw new ExportDataException(e.getCause());
            }
        }
    }

    /**
     * Creates a database connection to the Oracle SQL database.
     *
     * @return a Connection object representing the database connection
     * @throws SQLException if an SQL exception occurs during the connection
     *                      creation
     */
    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(this.url, Input.getStringInput("Enter your database username: "),
                Input.getPassword());
    }

    /**
     * Creates a vehicle object from the result sets obtained from the database.
     *
     * @param vehicle the result set containing vehicle data
     * @param type    the result set containing vehicle type data
     * @return a {@code Vehicle} object created from the result sets
     * @throws SQLException if an SQL exception occurs during the vehicle creation
     */
    private Vehicle createVehicleFromResult(ResultSet vehicle, ResultSet type) throws SQLException {
        if (type.getString("type").equals("Car")) {
            String brand = vehicle.getString("brand");
            String model = vehicle.getString("model");
            int year = vehicle.getInt("year");
            double price = vehicle.getDouble("price");
            String color = vehicle.getString("color");
            int horsePower = vehicle.getInt("horsePower");
            int doorCount = vehicle.getInt("doorCount");
            String fuelType = vehicle.getString("fuelType");
            double trunkCapacity = vehicle.getDouble("trunkCapacity");
            return new Car(brand, model, year, price, color, horsePower, doorCount, fuelType, trunkCapacity);
        } else {
            String brand = vehicle.getString("brand");
            String model = vehicle.getString("model");
            int year = vehicle.getInt("year");
            double price = vehicle.getDouble("price");
            String color = vehicle.getString("color");
            int horsePower = vehicle.getInt("horsePower");
            int maxAltitude = vehicle.getInt("maxAltitude");
            double maxCargoCapacity = vehicle.getDouble("maxCargoCapacity");
            int maxSeatingCapacity = vehicle.getInt("maxSeatingCapacity");
            return new Plane(brand, model, year, price, color, horsePower, maxAltitude, maxCargoCapacity,
                    maxSeatingCapacity);
        }
    }
}
