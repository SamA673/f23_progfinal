package motorstore.dataImport.importer;

import java.util.List;
import motorstore.datalogic.product.*;

/**
 * The IVehicleImporter interface defines methods for importing and exporting
 * vehicle data.
 * Classes implementing this interface should provide functionality to load and
 * save lists of vehicles.
 * These methods may throw specific exceptions related to data import and export
 * operations.
 *
 * @author David P
 * 
 */
public interface IVehicleImporter {
    /**
     * Loads a list of vehicles from a data source.
     *
     * @return a list of vehicles
     * @throws ImportDataException if an error occurs during the data import process
     */
    List<Vehicle> loadVehicles() throws ImportDataException;

    /**
     * Saves a list of vehicles to a data destination.
     *
     * @param vehicles the list of vehicles to be saved
     * @throws ExportDataException if an error occurs during the data export process
     */
    void saveVehicles(List<Vehicle> vehicles) throws ExportDataException;
}
