package motorstore.dataImport.importer;

/**
 * This class represents an exception that may be thrown during the
 * exporting of data in the inventory system.
 * It extends the class and provides constructors to handle
 * exceptions with a cause OR a custom message.
 *
 * @author Sam A
 */

public class ExportDataException extends Exception {
    /**
     * Constructs an exception with the specified cause.
     * 
     * @param cause the cause of the exception
     */
    public ExportDataException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs an exception with the specified custom message.
     * This constructor also prints the provided message to the standard output.
     *
     * @param message the custom message for the exception
     */
    public ExportDataException(String message) {
        System.out.println(message);
    }

}
