package motorstore.datalogic.uilogic;

import java.util.ArrayList;

import motorstore.dataImport.importer.ExportDataException;
import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.datalogic.InventorySystem;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.display.SortStrategy;
import motorstore.datalogic.display.filterStrategies.FilterBrand;
import motorstore.datalogic.display.filterStrategies.FilterCar;
import motorstore.datalogic.display.filterStrategies.FilterPlane;
import motorstore.datalogic.display.sortComparators.PriceComparator;
import motorstore.datalogic.display.sortComparators.YearComparator;
import motorstore.datalogic.product.*;
import motorstore.utilities.Input;
import motorstore.utilities.Utilities;

public class UILogic {
    /**
     * Gets a vehicle within an InventorySystem Vehicle List using position parameter and edits it
     * @param position Position of the Vehicle within vehicles List
     * @param invSys InventorySystem containing vehicles List to be edited
     */
    public static void editVehicleLogic(int position, InventorySystem invSys) {
        position -= 1;
        try {
            Vehicle vehicle = invSys.getVehicles().get(position);
            invSys.editVehicle(position, createEditedVehicle(vehicle));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Invalid position: " + position);
            throw e;
        }
    }

    /**
     * Creates a new Vehicle object of the same type as Vehicle in parameter
     * @param vehicle Vehicle which the type is to be copied
     * @return A new Car or Plane object depending on parameter object type
     */
    public static Vehicle createEditedVehicle(Vehicle vehicle) {
        if (vehicle instanceof Car) {
            return Input.createCar();
        } else {
            return Input.createPlane();
        }
    }

    /**
     * Adds a vehicle to the InventorySystem passed as parameter
     * @param vehicle Vehicle to be added to the InventorySystem vehicles List
     * @param invSys InventorySystem containing vehicles List to be added to
     */
    public static void addVehicleLogic(Vehicle vehicle, InventorySystem invSys) {
        invSys.addVehicle(vehicle);
    }

    /**
     * Returns an IVehicleDisplayer object depending on input
     * @param input User input to choose which Displayer to use
     * @param invSys InventorySystem containing the vehicle to be arranged/displayed
     * @return IVehicleDisplayer object of sort/filter depending on user input
     */
    public static IVehicleDisplayer displayVehicleInputLogic(int input, InventorySystem invSys) {
        switch (input) {
            case 1:
                return new FilterCar(invSys.getVehicles());
            case 2:
                return new FilterPlane(invSys.getVehicles());
            case 3:
                return new FilterBrand(invSys.getVehicles(),
                        Input.getStringInput("<SYSTEM> What brand are you looking for?"));
            case 4:
                return new SortStrategy(invSys.getVehicles(), new YearComparator());
            case 5:
                return new SortStrategy(invSys.getVehicles(), new PriceComparator());
            case 0:
                return null;
            default:
                throw new IllegalArgumentException("Option does not exist!");
        }
    }

    /**
     * Prints the vehicle List in InventorySystem in a formatted way according to displayer parameter
     * @param invSys InventorySystem with vehicles to be printed
     * @param displayer Displayer to sort/filter vehicle List
     */
    public static void displayVehiclePrintLogic(InventorySystem invSys, IVehicleDisplayer displayer) {
        Utilities.drawMenuHeaderLong();
        invSys.setVehicleDisplayer(displayer);
        Utilities.vehicleListPrinterFormatted(invSys.arrange());
    }

    /**
     * Returns a new Vehicle according to user input
     * @param input User input to choose which vehicle to create
     * @return New Vehicle
     */
    public static Vehicle addVehicleInputLogic(int input) {
        switch (input) {
            case 1:
                return Input.createCar();
            case 2:
                return Input.createPlane();
            case 0:
                return null;
            default:
                throw new IllegalArgumentException("<SYSTEM> Please enter a valid input '1', '2' or '0'");
        }
    }

    /**
     * Checks to see if email is within the InventorySystem loyaltyMembers map
     * @param invSys InventorySystem containing loyaltyMembers map
     * @param email Email to check within map
     * @return A boolean determining if the email was in the map or not
     */
    public static boolean checkIfRealCustomer(InventorySystem invSys, String email) {
        if (invSys.getLoyaltyMembers().containsKey(email)) {
            return true;
        }
        return false;
    }

    /**
     * Saves the vehicles from within an InventorySystem to a csv file
     * @param invSys InventorySystem containing vehicles List
     * @throws ExportDataException If there is an issue exporting vehicles List to the csv file
     */
    public static void saveLogic(InventorySystem invSys) throws ExportDataException {
        invSys.getVehicleImporter().saveVehicles(invSys.getVehicles());
        if (invSys.getVehicleImporter() instanceof FileJavaImporter) {
            ((FileJavaImporter) invSys.getVehicleImporter()).saveLoyalty(invSys.getLoyaltyMembers());
        }
    }
}
