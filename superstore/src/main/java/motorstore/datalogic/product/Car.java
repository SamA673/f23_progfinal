package motorstore.datalogic.product;

import java.sql.*;
import java.util.Map;

import motorstore.utilities.Input;

public class Car extends Vehicle implements SQLData {
    private static final String TYPE_NAME="CARTYP";
    private int doorCount;
    private String fuelType;
    private double trunkCapacity;

    /**
     * Constructor for Car using individual fields as parameters
     * Sets the brand, model, year, price, color, and horsepower to Vehicle class using its constructor
     * Sets doorCount, fuelType, trunkCapacity to local fields
     * @param brand Brand of the car
     * @param model Model of the car
     * @param year Production year of the car
     * @param price Cost of the car
     * @param color Colour of the car
     * @param horsePower Horsepower of the car
     * @param doorCount Door count of the car
     * @param fuelType Type of fuel car uses
     * @param trunkCapacity Trunk capacity of the car
     */
    public Car(String brand, String model, int year, double price, String color, int horsePower, int doorCount, String fuelType, double trunkCapacity) {
        super(brand, model, year, price, color, horsePower);
        this.doorCount = doorCount;
        this.fuelType = fuelType;
        this.trunkCapacity = trunkCapacity;
    }

    /**
     * Constructor for Car using String[] array
     * @param fields All the fields in String[] format
     */
    public Car(String[] fields) {
        super(fields[1], fields[2], Integer.parseInt(fields[3]), Double.parseDouble(fields[4]), fields[5], Integer.parseInt(fields[6]));
        this.doorCount = Integer.parseInt(fields[7]);
        this.fuelType = fields[8];
        this.trunkCapacity = Double.parseDouble(fields[9]);
    }

    // Getters
    public int getDoorCount() {
        return doorCount;
    }

    public String getFuelType() {
        return fuelType;
    }

    public double getTrunkCapacity() {
        return trunkCapacity;
    }

    // Setters
    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public void setTrunkCapacity(double trunkCapacity) {
        this.trunkCapacity = trunkCapacity;
    }

    /**
     * Returns a string format of the Car object
     * @return A Car object formatted as a String
     */
    public String toString() {
        String myCar = this.getBrand() + " " + this.getModel() + " (Car)" + '\n';
        myCar += "Year: " + this.getYear() + '\n';
        myCar += "Price (USD): " + this.getPrice() + '\n';
        myCar += "Color: " + this.getColor() + '\n';
        myCar += "Horsepower: " + this.getHorsePower() + '\n';
        myCar += "Door count: " + this.doorCount + '\n';
        myCar += "Fuel Type: " + this.fuelType + '\n';
        myCar += "Trunk Capacity (L): " + this.trunkCapacity + '\n';
        return myCar;
    }

    /**
     * Returns a csv formatted string of the Car object
     * @return A Car object formatted into csv format
     */
    public String export() {
        return "Car," + this.getBrand() + "," + this.getModel() + "," + this.getYear() + "," + this.getPrice() + ","
                + this.getColor() + "," + this.getHorsePower() + ","
                + this.doorCount + "," + this.fuelType + "," + this.trunkCapacity;
    }

    /**
     * Returns the SQL type name of the Car in the fields
     * @return The SQL type name of the Car
     */
    @Override
    public String getSQLTypeName() throws SQLException {
        return Car.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setBrand(stream.readString());
        setModel(stream.readString());
        setYear(stream.readInt());
        setPrice(stream.readDouble());
        setColor(stream.readString());
        setHorsePower(stream.readInt());
        setDoorCount(stream.readInt());
        setFuelType(stream.readString());
        setTrunkCapacity(stream.readDouble());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getBrand());
        stream.writeString(getModel());
        stream.writeInt(getYear());
        stream.writeDouble(getPrice());
        stream.writeString(getColor());
        stream.writeInt(getHorsePower());
        stream.writeInt(getDoorCount());
        stream.writeString(getFuelType());
        stream.writeDouble(getTrunkCapacity());
    }

    /**
     * Adds a Car to the database using stored procedures within SQL
     * @param conn Connection to the database
     */
    @Override
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(TYPE_NAME, Class.forName("motorstore.datalogic.product.Car"));
        String sql = "{call motorstore_package.insert_Car(?,?)}";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, "Car");
            stmt.setObject(2, this);
            stmt.execute();
        }
    }
}