package motorstore.datalogic.product;

import java.sql.*;
import java.util.Map;

public class Plane extends Vehicle implements SQLData {
    private static final String TYPE_NAME="PLATYP";
    private int maxAltitude;
    private double maxCargoCapacity;
    private int maxSeatingCapacity;

    /**
     * Constructor for Plane using individual fields as parameters
     * Sets the brand, model, year, price, color, and horsepower to Vehicle class using its constructor
     * Sets maxAltitude, maxCargoCapacity, maxSeatingCapacity to local fields
     * @param brand Brand of the plane
     * @param model Model of the plane
     * @param year Production year of the plane
     * @param price Cost of the plane
     * @param color Colour of the plane
     * @param horsePower Horsepower of the plane
     * @param maxAltitude Max altitude of the plane
     * @param maxCargoCapacity Max cargo capacity of the plane
     * @param maxSeatingCapacity Max seating capacity of the plane
     */
    public Plane(String brand, String model, int year, double price, String color,
            int horsePower, int maxAltitude, double maxCargoCapacity, int maxSeatingCapacity) {
        super(brand, model, year, price, color, horsePower);
        this.maxAltitude = maxAltitude;
        this.maxCargoCapacity = maxCargoCapacity;
        this.maxSeatingCapacity = maxSeatingCapacity;
    }

    /**
     * Constructor for Plane using String[] array
     * @param fields All the fields in String[] format
     */
    public Plane(String[] fields)  {
        super(fields[1], fields[2], Integer.parseInt(fields[3]), Double.parseDouble(fields[4]), fields[5],
                Integer.parseInt(fields[6]));
            this.maxAltitude = Integer.parseInt(fields[7]);
            this.maxCargoCapacity = Double.parseDouble(fields[8]);
            this.maxSeatingCapacity = Integer.parseInt(fields[9]);
    }

    // Getters
    public int getMaxAltitude() {
        return maxAltitude;
    }

    public double getMaxCargoCapacity() {
        return maxCargoCapacity;
    }

    public int getMaxSeatingCapacity() {
        return maxSeatingCapacity;
    }

    // Setters
    public void setMaxAltitude(int maxAltitude) {
        this.maxAltitude = maxAltitude;
    }

    public void setMaxCargoCapacity(double maxCargoCapacity) {
        this.maxCargoCapacity = maxCargoCapacity;
    }

    public void setMaxSeatingCapacity(int maxSeatingCapacity) {
        this.maxSeatingCapacity = maxSeatingCapacity;
    }

    /**
     * Returns a string format of the Plane object
     * @return A Plane object formatted as a String
     */
    public String toString() {
        String myPlane = this.getBrand() + " " + this.getModel() + " (Plane)" + '\n';
        myPlane += "Year: " + this.getYear() + '\n';
        myPlane += "Price (USD): " + String.format("%.2f", this.getPrice()) + '\n'; // String.format removes scientific
                                                                                    // notation for big prices
        myPlane += "Color: " + this.getColor() + '\n';
        myPlane += "Horsepower: " + this.getHorsePower() + '\n';
        myPlane += "Max Altitude (ft): " + this.maxAltitude + '\n';
        myPlane += "Max Cargo Capacity (Tons): " + this.maxCargoCapacity + '\n';
        myPlane += "Max Seating Capacity: " + this.maxSeatingCapacity + '\n';

        return myPlane;
    }

    /**
     * Returns a csv formatted string of the Plane object
     * @return A Plane object formatted into csv format
     */
    public String export() {
        return "Plane," + this.getBrand() + "," + this.getModel() + "," + this.getYear() + "," + this.getPrice() + ","
                + this.getColor() + "," + this.getHorsePower() + ","
                + this.maxAltitude + "," + this.maxCargoCapacity + "," + this.maxSeatingCapacity;
    }

    /**
     * Returns the SQL type name of the Plane in the fields
     * @return The SQL type name of the Plane
     */
    @Override
    public String getSQLTypeName() throws SQLException {
        return Plane.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setBrand(stream.readString());
        setModel(stream.readString());
        setYear(stream.readInt());
        setPrice(stream.readDouble());
        setColor(stream.readString());
        setHorsePower(stream.readInt());
        setMaxAltitude(stream.readInt());
        setMaxCargoCapacity(stream.readDouble());
        setMaxSeatingCapacity(stream.readInt());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getBrand());
        stream.writeString(getModel());
        stream.writeInt(getYear());
        stream.writeDouble(getPrice());
        stream.writeString(getColor());
        stream.writeInt(getHorsePower());
        stream.writeInt(getMaxAltitude());
        stream.writeDouble(getMaxCargoCapacity());
        stream.writeInt(getMaxSeatingCapacity());
    }

    /**
     * Adds a Plane to the database using stored procedures within SQL
     * @param conn Connection to the database
     */
    @Override
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(TYPE_NAME, Class.forName("motorstore.datalogic.product.Plane"));
        String sql = "{call motorstore_package.insert_Plane(?,?)}";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, "Plane");
            stmt.setObject(2, this);
            stmt.execute();
        }
    }
}
