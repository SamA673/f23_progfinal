package motorstore.datalogic.product;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class Vehicle {
    private String brand;
    private String model;
    private int year;
    private double price;
    private String color;
    private int horsePower;

    // Constructor
    /**
     * Constructor for Vehicle objects
     * @param brand Brand of vehicle
     * @param model Model of vehicle
     * @param year Product year of the vehicle
     * @param price Cost of the vehicle
     * @param color Colour of the vehicle
     * @param horsePower Horsepower of the vehicle
     */
    public Vehicle(String brand, String model, int year, double price, String color, int horsePower) {
            this.brand = brand;
            this.model = model;
            this.year = year;
            this.price = price;
            this.color = color;
            this.horsePower = horsePower;
    }

    // Getters
    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    public int getHorsePower() {
        return horsePower;
    }

    // Setters
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    /**
     * Returns a csv formatted string of the Vehicle object
     * @return A vehicle object formatted into csv format
     */
    public abstract String export();

    /**
     * Adds a Vehicle to the database using stored procedures within SQL
     * @param conn Connection to the database
     * @throws SQLException If there was an error adding the object to the database
     * @throws ClassNotFoundException If the Vehicle object class could not be found
     */
    public abstract void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException;
}
