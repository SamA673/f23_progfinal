package motorstore.datalogic.display.filterStrategies;
import java.util.*;

import motorstore.datalogic.display.FilterStrategy;
import motorstore.datalogic.product.*; 


public class FilterPlane extends FilterStrategy{
    /**
     * Constructor for FilterPlane
     * Sets the vehicles field in FilterStrategy
     * @param vehicles List of Vehicles to copy
     */
    public FilterPlane(List<Vehicle> vehicles){
        super(vehicles); 
    }
    
    /**
     * Arranges a List of vehicles
     * @return Filters the copy List of vehicles by Plane type
     */
    public List<Vehicle> arrange(){
        List<Vehicle> filteredVehicles = new ArrayList<Vehicle>(); 
        for (Vehicle v : this.getVehicles()){
            if(v instanceof Plane){
                filteredVehicles.add(v); 
            }
        }
        return filteredVehicles;
    }
}