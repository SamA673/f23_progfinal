package motorstore.datalogic.display.filterStrategies;

import motorstore.datalogic.display.FilterStrategy;
import motorstore.datalogic.product.Vehicle;

import java.util.*;

public class FilterBrand extends FilterStrategy {
    private String selectedBrand;

    /**
     * Constructor for FilterBrand
     * Sets the vehicles field in FilterStrategy
     * Set the selectedBrand local field
     * @param vehicles List of Vehicles to copy
     * @param brand Brand to filter by
     */
    public FilterBrand(List<Vehicle> vehicles, String brand) {
        super(vehicles);
        this.selectedBrand = brand;
    }

    /**
     * Arranges a List of vehicles
     * @return Filters the copy List of vehicles by brand
     */
    public List<Vehicle> arrange() {
        List<Vehicle> filteredVehicles = new ArrayList<Vehicle>();
        for (Vehicle v : this.getVehicles()) {
            if (v.getBrand().equals(this.selectedBrand)) {
                filteredVehicles.add(v);
            }
        }
        return filteredVehicles;
    }
}