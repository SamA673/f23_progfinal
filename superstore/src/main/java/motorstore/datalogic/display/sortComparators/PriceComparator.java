package motorstore.datalogic.display.sortComparators;

import java.util.Comparator;

import motorstore.datalogic.product.Vehicle;

public class PriceComparator implements Comparator<Vehicle> {
    /**
     * Compares 2 vehicles using the Double.compare method in ascending order
     * @return An integer saying if first Vehicle price is larger, smaller or equal to second Vehicle price
     */
    @Override
    public int compare(Vehicle v1, Vehicle v2) {
        return Double.compare(v1.getPrice(), v2.getPrice());
    }
}
