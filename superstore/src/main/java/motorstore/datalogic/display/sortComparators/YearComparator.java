package motorstore.datalogic.display.sortComparators;

import java.util.Comparator;

import motorstore.datalogic.product.Vehicle;

public class YearComparator implements Comparator<Vehicle> {
    /**
     * Compares 2 vehicles using the Integer.compare method in descending order
     * @return An integer saying if first Vehicle year is larger, smaller or equal to second Vehicle year
     */
    @Override
    public int compare(Vehicle v1, Vehicle v2) {
        return Integer.compare(v2.getYear(), v1.getYear());
    }
}
