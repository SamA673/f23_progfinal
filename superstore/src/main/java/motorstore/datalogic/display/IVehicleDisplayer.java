package motorstore.datalogic.display;

import java.util.List;
import motorstore.datalogic.product.Vehicle;

public interface IVehicleDisplayer {
    /**
     * Arranges a List of vehicles
     * @return Sorted or filtered copy List of vehicles
     */
    List<Vehicle> arrange();
}
