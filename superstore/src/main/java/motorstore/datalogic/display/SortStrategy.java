package motorstore.datalogic.display;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import motorstore.datalogic.product.Vehicle;

public class SortStrategy implements IVehicleDisplayer {
    private Comparator<Vehicle> comparator;
    private List<Vehicle> sortedVehicles;

    /**
     * Constructor for SortStrategy
     * Sets the sortedVehicles List using a deep copy of the vehicles parameter
     * Sets the comparator Comparator
     * @param vehicles List of vehicles to sort
     * @param comparator Comparator to sort list of vehicles by
     */
    public SortStrategy(List<Vehicle> vehicles, Comparator<Vehicle> comparator) {
        List<Vehicle> sortedVehicles = new ArrayList<Vehicle>();
        for (Vehicle vehicle : vehicles) {
            sortedVehicles.add(vehicle);
        }
        this.sortedVehicles = sortedVehicles;
        this.comparator = comparator;
    }
    
    /**
     * Arranges a List of vehicles
     * @return Sorts the List of vehicles
     */
    public List<Vehicle> arrange() {
        Collections.sort(this.sortedVehicles, this.comparator);
        return this.sortedVehicles;
    }
}
