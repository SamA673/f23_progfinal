package motorstore.datalogic.display;

import java.util.*;

import motorstore.datalogic.product.Vehicle;

public abstract class FilterStrategy implements IVehicleDisplayer {
    private List<Vehicle> vehicles;

    /**
     * Constructor for FilterStrategy
     * Sets the vehicles field
     * @param vehicles List of vehicles to set vehicles field with
     */
    public FilterStrategy(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    // Getter
    public List<Vehicle> getVehicles(){
        return this.vehicles; 
    }
}