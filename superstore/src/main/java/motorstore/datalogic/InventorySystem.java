package motorstore.datalogic;

import java.util.List;

import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.dataImport.importer.IVehicleImporter;
import motorstore.dataImport.importer.ImportDataException;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.product.Vehicle;
import java.util.HashMap;
import java.util.Map;

public class InventorySystem {
    private List<Vehicle> vehicles;
    private IVehicleDisplayer vehicleDisplayer;
    private IVehicleImporter dataLoader;
    private Map<String, List<String>> loyaltyMembers;

    /**
     * Constructor for InventorySystem
     * Sets the vehicles List
     * Sets the dataLoader using the importer provided
     * @param dataLoader A vehicle importer that can be used to load and save the inventory to the resource files
     * @throws ImportDataException
     */
    public InventorySystem(IVehicleImporter dataLoader) throws ImportDataException {
        this.vehicles = dataLoader.loadVehicles();
        this.dataLoader = dataLoader;
        if (this.dataLoader instanceof FileJavaImporter) {
            this.loyaltyMembers = ((FileJavaImporter) dataLoader).loadLoyalty();
        } else {
            this.loyaltyMembers = new HashMap<String, List<String>>();
        }
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public IVehicleDisplayer getVehicleDisplayer() {
        return this.vehicleDisplayer;
    }

    /**
     * Edits a Vehicle within the vehicles List
     * @param position Position within the vehicles List
     * @param vehicle The Vehicle that will replace current Vehicle in List
     */
    public void editVehicle(int position, Vehicle vehicle) {
        this.vehicles.set(position, vehicle);
    }

    public IVehicleImporter getVehicleImporter() {
        return this.dataLoader;
    }

    public Map<String, List<String>> getLoyaltyMembers() {
        return this.loyaltyMembers;
    }

    /**
     * Sets the displayer using a sorter or filter
     * @param displayer Type of displayer set to vehicles List
     */
    public void setVehicleDisplayer(IVehicleDisplayer displayer) {
        this.vehicleDisplayer = displayer;
    }

    /**
     * Adds a vehicle to the vehicles List
     * @param vehicle Vehicle to add to vehicles List
     */
    public void addVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
    }

    /**
     * Removes a vehicle from the vehicles List
     * @param position Position of the Vehicle to remove
     */
    public void removeVehicle(int position) {
        vehicles.remove(position);
    }

    /**
     * Sorts or filters the vehicles List according to sort or filter given in displayer
     * @return Sorted or filtered copy List of the vehicles List
     */
    public List<Vehicle> arrange() {
        return this.vehicleDisplayer.arrange();
    }
}
