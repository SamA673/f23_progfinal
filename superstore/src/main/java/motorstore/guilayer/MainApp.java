package motorstore.guilayer;

import java.util.Scanner;

import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.dataImport.importer.IVehicleImporter;
import motorstore.dataImport.importer.ImportDataException;
import motorstore.datalogic.InventorySystem;
import motorstore.utilities.Input;
import motorstore.utilities.Utilities;

/**
 * The MainApp class serves as the entry point for the MotorStore application.
 * It initializes the necessary components, such as the InventorySystem,
 * and prompts
 * the user to identify whether they are an admin or a regular user. Based on
 * the user's response,
 * it launches the appropriate application (AdminApp or EmployeeApp).
 *
 * @author David P & Sam A
 * 
 */
public class MainApp {

    /**
     * Runs the MotorStore application.
     *
     * @throws ImportDataException if an error occurs during data import
     */
    public static void runApplication() throws ImportDataException {
        IVehicleImporter importer = new FileJavaImporter("superstore/src/main/resources/file.txt",
                "superstore/src/main/resources/loyalty.txt");
        InventorySystem invSys = new InventorySystem(importer);

        AppUI app = initializeApp(invSys);
        app.run();
    }

    /**
     * Asks user if they are an admin or employee and returns a different app according to response
     * @param invSys InventorySystem passed to new app
     * @return Admin or Employee app depending on user input
     */
    private static AppUI initializeApp(InventorySystem invSys) {
        while (true) {
            Utilities.drawMenuHeader();
            String response = Input.getStringInput("<SYSTEM> Are you an admin? ('y' if yes, 'n' if no!)");

            switch (response) {
                case "y":
                    return new AdminApp(invSys);
                case "n":
                    return new EmployeeApp(invSys);
                default:
                    System.out.println("<SYSTEM> You may only input 'y' or 'n'... Try again. ");
                    Input.waitForInput();
            }
        }
    }
}
