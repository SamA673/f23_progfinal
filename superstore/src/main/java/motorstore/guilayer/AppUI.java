package motorstore.guilayer;

import java.util.ArrayList;

import motorstore.dataImport.importer.ExportDataException;
import motorstore.datalogic.InventorySystem;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.product.Vehicle;
import motorstore.datalogic.uilogic.UILogic;
import motorstore.utilities.Input;
import motorstore.utilities.Utilities;

/**
 * The AppUI class is an abstract class representing the user interface for the
 * MotorStore application.
 * It provides common methods and structure for both the AdminApp and
 * EmployeeApp classes.
 * It includes methods for creating orders, displaying vehicles, and saving
 * data.
 *
 * @author David P & Sam A
 */
public abstract class AppUI {
    protected InventorySystem invSys;

    /**
     * Constructs an instance of AppUI with the specified InventorySystem.
     * 
     * @param invSys the InventorySystem instance
     */
    public AppUI(InventorySystem invSys) {
        this.invSys = invSys;
    }

    /**
     * Abstract method to be implemented by subclasses to run the specific
     * functionality of the application.
     */
    protected abstract void run();

    /**
     * Abstract method to be implemented by subclasses to run the main menu and
     * process user choices.
     */
    protected abstract void runMainMenuUserChoice();

    /**
     * Method to handle the user interface for creating an order.
     */
    protected void createOrderUI() {

        try {
            Utilities.drawOrderMenu(invSys.getVehicles());
            Vehicle purchase;
            double discountMultiplier = 1;

            int response = Input.getIntInput("What would you like to buy? (Enter number associated with listing)") - 1;
            purchase = invSys.getVehicles().get(response);

            Utilities.drawMenuHeader();
            String email = Input.getStringInput("<SYSTEM> What is your email?");

            if (Utilities.checkIfRealCustomer(invSys, email)) {
                System.out.println("<SYSTEM> Looks like you're already a MotorStore++ Loyalty Premium member!");
                discountMultiplier = Utilities.calculateDiscount(invSys, purchase, email);
            } else {
                System.out.println("<SYSTEM> Uh oh... We noticed you're not already a member.");
                System.out.println(
                        "<SYSTEM> We have added you to the MotorStore++ Premium (Powered by VISA) membership program ($64.99/year!)");
                System.out.println("<SYSTEM> To unsubscribe, give us a call at our 24/7 call center! (1800-123-2452)");
                invSys.getLoyaltyMembers().put(email, new ArrayList<String>());
                Input.waitForInput();
            }

            orderFinalizing(purchase, email, discountMultiplier);

        } catch (IndexOutOfBoundsException e) {
            System.out.println("<SYSTEM> That vehicle does not exist... Aborting.");
            Input.waitForInput();
        }

    }

    /**
     * Method to handle the user interface for displaying inventory.
     */
    protected void displayUI() {
        boolean isDone = false;

        while (!isDone) {
            Utilities.drawDisplayVehicleMenu(this.invSys.getVehicles());

            int response = Input.getIntInput('\n' + "<SYSTEM> Please enter a value.");

            try {
                IVehicleDisplayer displayer = UILogic.displayVehicleInputLogic(response, invSys);
                if (displayer == null) {
                    isDone = true;
                } else {
                    UILogic.displayVehiclePrintLogic(invSys, displayer);
                    Input.waitForInput();
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Option does not exist!");
                Input.waitForInput();
            }
            Utilities.flushScreen();
        }
    }

    /**
     * Method to save vehicles onto a file, db, or other.
     */
    protected void save() {
        try {
            UILogic.saveLogic(this.invSys);
            System.out.println("Success!");
            Input.waitForInput();
        } catch (ExportDataException e) {
            Input.waitForInput();
        }
    }

    /**
     * Method to handle the finalization of a vehicle purchase in the user
     * interface.
     * It displays information about the selected vehicle, including any applicable
     * discounts,
     * and prompts the user for confirmation before completing the purchase.
     *
     * @param purchase           the selected vehicle for purchase
     * @param email              the email of the customer
     * @param discountMultiplier the discount multiplier applied to the purchase
     */
    protected void orderFinalizing(Vehicle purchase, String email, double discountMultiplier) {
        Utilities.drawMenuHeader();
        System.out.println(purchase);
        System.out.println('\n' + purchase.getBrand() + " " + purchase.getModel() + " (" + purchase.getColor()
                + "): $" + purchase.getPrice());
        System.out.println(
                "Discount: $" + Math.round(purchase.getPrice() - purchase.getPrice() * discountMultiplier));
        System.out.println("Subtotal: $" + Math.round((purchase.getPrice() * discountMultiplier) * 100) / 100);

        String answer = Input
                .getStringInput("<SYSTEM> Are you sure you want to purchase this vehicle? (y for yes) " + '\n');

        if (answer.equals("y")) {
            invSys.getLoyaltyMembers().get(email).add(purchase.getBrand());
            System.out.println("<SYSTEM> Purchase complete!");
        } else {
            System.out.println("<SYSTEM> You're missing out... I heard " + purchase.getBrand()
                    + " makes the best vehicles on the market...");
        }
        Input.waitForInput();

    }
}
