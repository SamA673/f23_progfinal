package motorstore.guilayer;

import java.io.Console;
import motorstore.datalogic.InventorySystem;
import motorstore.datalogic.product.*;
import motorstore.datalogic.uilogic.UILogic;
import motorstore.utilities.Input;
import motorstore.utilities.Utilities;

/**
 * Class representing the user interface for the admin user.
 * It extends the AppUI class and includes specific functionalities for admin
 * users.
 * 
 * @Author David P & Sam A
 */
public class AdminApp extends AppUI {
    private static final String adminPW = "123ILOVECARS";

    /**
     * Constructor for the AdminApp class.
     *
     * @param invSys The InventorySystem associated with the admin user interface.
     */
    public AdminApp(InventorySystem invSys) {
        super(invSys);
    }

    /**
     * Method to execute the main functionality of the admin application.
     * It initiates the login process and runs the main menu based on successful
     * authentication.
     */
    protected void run() {
        login();
        runMainMenuUserChoice();
    }

    /**
     * Method to authenticate the user by comparing the provided password with the
     * admin password.
     *
     * @param password The password entered by the user for authentication.
     * @return true if authentication is successful, false otherwise.
     */
    private boolean authenticate(String password) {
        if (password.equals(adminPW)) {
            return true;
        }
        return false;
    }

    /**
     * Method to handle the login process for the admin user.
     * It prompts the user to enter the admin password and continues until
     * successful authentication.
     */
    private void login() {
        boolean isTrusted = false;
        Console console = System.console();
        while (!isTrusted) {
            Utilities.drawMenuHeader();
            isTrusted = authenticate(String.valueOf(console.readPassword("<SYSTEM> Please enter the admin password:\n")));
            if (!isTrusted) {
                System.out.println("<SYSTEM> Wrong. Try again...");
                Input.waitForInput();
            }
        }
    }

    /**
     * Method to handle the user's choice in the main menu for an admin user.
     */
    @Override
    protected void runMainMenuUserChoice() {
        boolean isDone = false;
        while (!isDone) {
            Utilities.flushScreen();
            Utilities.drawAdminMenu();
            int response = Input.getIntInput("<SYSTEM> Please enter a value.");
            if (response == 1) {
                createOrderUI();
            } else if (response == 2) {
                displayUI();
            } else if (response == 3) {
                addVehicleUI();
            } else if (response == 4) {
                editVehicleUI();
            } else if (response == 5) {
                deleteVehicleUI();
            } else if (response == 6) {
                save();
            } else if (response == 0) {
                isDone = true;
            } else {
                System.out.println(" <SYSTEM> Enter a valid number corresponding to an option on the menu. ");
                Input.waitForInput();
            }
        }
    }

    /**
     * Method to handle the user interface for editing a vehicle.
     * It displays a formatted list of vehicles and prompts the user to select a
     * vehicle for editing.
     * The input logic is processed to edit the selected vehicle's details.
     */
    private void editVehicleUI() {
        Utilities.drawEditVehicleMenu(this.invSys.getVehicles());

        int response = Input.getIntInput("Input the vehicle you wish to edit (Enter 0 if you wish to exit): ");
        if (response != 0) {
            try {
                UILogic.editVehicleLogic(response, invSys);
            } catch (IndexOutOfBoundsException e) {
                editVehicleUI();
            }
        }
        System.out.println("Successful!");
        System.out.println("Returning to main menu...");
        Input.waitForInput();
    }

    /**
     * Method to handle the user interface for adding a vehicle.
     * It displays a menu for selecting the type of vehicle to add and prompts the
     * user to provide the necessary details.
     */
    private void addVehicleUI() {
        boolean isDone = false;

        while (!isDone) {
            Utilities.drawAddVehicleMenu();

            int response = Input.getIntInput('\n' + "<SYSTEM> Please enter a value.");

            try {
                Vehicle vehicle = UILogic.addVehicleInputLogic(response);
                if (vehicle == null) {
                    isDone = true;
                } else {
                    UILogic.addVehicleLogic(vehicle, this.invSys);
                    System.out.println("Success!");
                    Input.waitForInput();
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Option does not exist!");
                Input.waitForInput();
            }
        }
    }

    /**
     * Method to handle the user interface for deleting a vehicle.
     * It displays a formatted list of vehicles and prompts the user to select a
     * vehicle for removal.
     *
     * @throws IndexOutOfBoundsException when the selected index is out of bounds...
     */
    private void deleteVehicleUI() {
        try {
            Utilities.vehicleListPrinterFormatted(this.invSys.getVehicles());
            int position = Input.getIntInput("Which vehicle would you like to remove? (or 0 to exit) ");
            if (position == 0)
                return;
            this.invSys.removeVehicle(position - 1);
            System.out.println("<SYSTEM> Success!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("<SYSTEM> That index does not exist. Aborting.");
            Input.waitForInput();
        }
    }
}
