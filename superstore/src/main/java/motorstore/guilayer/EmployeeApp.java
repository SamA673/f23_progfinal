package motorstore.guilayer;

import motorstore.datalogic.InventorySystem;
import motorstore.utilities.Input;
import motorstore.utilities.Utilities;

/**
 * The EmployeeApp class represents the user interface for an employee in the
 * MotorStore application.
 * It provides options for creating orders, displaying vehicle information, and
 * saving data.
 * This class extends AppUI!
 *
 * @author David P.
 */
public class EmployeeApp extends AppUI {

    /**
     * Constructs an instance of EmployeeApp with the specified InventorySystem.
     *
     * @param invSys the InventorySystem instance
     */
    public EmployeeApp(InventorySystem invSys) {
        super(invSys);
    }

    /**
     * Runs the main functionality of the EmployeeApp.
     */
    @Override
    protected void run() {
        runMainMenuUserChoice();
    }

    /**
     * Runs the main menu for the employee, processing user choices.
     */
    @Override
    protected void runMainMenuUserChoice() {
        boolean isDone = false;
        while (!isDone) {
            Utilities.flushScreen();
            Utilities.drawEmployeeMenu();
            int response = Input.getIntInput("<SYSTEM> Please enter a value.");
            if (response == 1) {
                createOrderUI();
            } else if (response == 2) {
                displayUI();
            } else if (response == 3) {
                save();
            } else if (response == 0) {
                isDone = true;
            } else {
                System.out.println(" <SYSTEM> Enter a valid number corresponding to an option on the menu. ");
                Input.waitForInput();
            }
        }
    }
}
