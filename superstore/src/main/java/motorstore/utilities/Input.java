package motorstore.utilities;

import motorstore.datalogic.product.*;

import java.io.Console;
import java.util.Scanner;

/**
 * The Input class provides utility methods for obtaining various types
 * of user inputs,
 * such as integers, strings, and doubles. It also includes methods to create
 * car and plane objects
 * based on user input.
 * 
 * @author Sam A
 * 
 */
public class Input {
    private static Scanner scan = new Scanner(System.in);

    /* ------------- VARIABLE INPUTS ------------- */

    /**
     * Gets an integer input from the user.
     * 
     * @param message Message to be printed before asking the user to input
     *                something.
     * @return int Inputted integer.
     */

    public static int getIntInput(String message) {
        System.out.println(message);
        while (true) {
            try {
                return Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("<SYSTEM> Please input an integer.");
            }
        }
    }

    /**
     * Gets a String input from the user.
     * 
     * @param message Message to be printed before asking the user to input
     *                something.
     * @return double inputted double
     */

    public static String getStringInput(String message) {
        System.out.println(message);
        return scan.nextLine();
    }

    /**
     * Gets a double input from the user.
     * 
     * @param message Message to be printed before asking the user to input
     *                something.
     * @return double inputted double
     */

    public static double getDoubleInput(String message) {
        System.out.println(message);
        while (true) {
            try {
                return Double.parseDouble(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("<SYSTEM> Please input a double.");
            }
        }
    }

    /* ------------- WAITER ------------- */

    public static void waitForInput() {
        System.out.print("(SYSTEM) Press enter to continue");
        scan.nextLine();
    }

    /* ------------- CREATERS ------------- */

    /**
     * Gets inputs from the the user and returns a car that is created using those
     * fields.
     * 
     * @param message Message to be printed before asking the user to input
     *                something.
     * @return Vehicle Newly created car
     */

    public static Vehicle createCar() {
        Utilities.drawMenuHeader();

        String brand = getStringInput("What is your car's brand?");
        String model = getStringInput("What is your car's model?");
        int year = getIntInput("What is your car's release year?");
        double price = getDoubleInput("What is your car's price?");
        String color = getStringInput("What is your car's color?");
        int horsepower = getIntInput("What is your car's horsepower?");
        int doorCount = getIntInput("How many doors?");
        String fuel = getStringInput("What is your car's fuel type?(IE: Gasoline, Electricity...etc)");
        double trunkCapacity = getDoubleInput("What is your car's trunk capacity (in Liters)");

        return new Car(brand, model, year, price, color, horsepower, doorCount, fuel, trunkCapacity);
    }

    /**
     * Gets inputs from the the user and returns a plane that is created using those
     * fields.
     * 
     * @param message Message to be printed before asking the user to input
     *                something.
     * @return Vehicle Newly created plane
     */

    public static Vehicle createPlane() {
        Utilities.drawMenuHeader();

        String brand = getStringInput("What is your plane's brand?");
        String model = getStringInput("What is your plane's model?");
        int year = getIntInput("What is your car's release year?");
        double price = getDoubleInput("What is your plane's price?");
        String color = getStringInput("What is your plane's color?");
        int horsepower = getIntInput("What is your plane's horsepower?");
        int maxAltitude = getIntInput("What is your plane's max altitude");
        double maxCargoCapacity = getDoubleInput("What is your plane's maximum cargo capacity? (in Liters)");
        int maxSeatingCapacity = getIntInput("What is your maximum seating capacity?");

        return new Plane(brand, model, year, price, color, horsepower, maxAltitude, maxCargoCapacity,
                maxSeatingCapacity);
    }

    /**
     * Gets the database password from the user in a secure manner.
     *
     * @return String Database password
     */
    public static String getPassword() {
        Console console = System.console();
        char[] passwordArray = console.readPassword("Enter your database password: ");
        return new String(passwordArray);
    }

}
