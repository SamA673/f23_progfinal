package motorstore.utilities;

import motorstore.datalogic.InventorySystem;
import motorstore.datalogic.product.*;

import java.util.List;

/**
 * The Utilities class provides utility methods for various tasks related to
 * user interface and data presentation.
 * It includes methods for drawing menus, displaying lists of vehicles,
 * formatting output, and calculating discounts.
 * The class is designed to enhance the overall UX in in MotorStore...
 *
 * @author Sam A.
 */
public class Utilities {

    /**
     * Clears the screen by printing escape characters.
     */
    public static void flushScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    /**
     * Draws the main menu for the MotorStore application.
     */
    public static void drawMenu() {
        drawMenuHeader();
        System.out.println("What do you wanna do?");
        System.out.println("1. Load the database");
        System.out.println("2. Display all vehicles");
        System.out.println("0. Log out!");
    }

    /**
     * Draws the menu header for the MotorStore application.
     */
    public static void drawMenuHeader() {
        flushScreen();
        System.out.println("<------------ Welcome to MotorStore ------------>");
        System.out.println("                (Version 2.0)                    " + '\n');
    }

    /**
     * Draws a long menu header for the MotorStore application.
     */
    public static void drawMenuHeaderLong() {
        flushScreen();
        System.out.println(
                "<----------------------------------------------- Welcome to MotorStore ----------------------------------------------->");
        System.out.println(
                "                                                    (Version 2.0)                    " + '\n');
    }

    /**
     * Draws the main menu for admins.
     */
    public static void drawAdminMenu() {
        drawMenuHeader();
        System.out.println("Hello Admin!");
        System.out.println("What would you like to do...?");
        System.out.println("(1) Create an order!");
        System.out.println("(2) Display");
        System.out.println("(3) Add Vehicle");
        System.out.println("(4) Edit Vehicle");
        System.out.println("(5) Delete Vehicle");
        System.out.println("(6) Save");
        System.out.println("(0) Exit!");
    }

    /**
     * Draws the main menu for employees
     */
    public static void drawEmployeeMenu() {
        drawMenuHeader();
        System.out.println("Hello!");
        System.out.println("What would you like to do...?");
        System.out.println("(1) Create an order!");
        System.out.println("(2) Display");
        System.out.println("(3) Save");
        System.out.println("(0) Exit!");
    }

    /**
     * Draws the menu for displaying.
     * 
     * @param vehicles list of vehicles to show initially
     */
    public static void drawDisplayVehicleMenu(List<Vehicle> vehicles) {
        drawMenuHeaderLong();
        System.out.println("Here is a list of all your vehicles.");
        vehicleListPrinterFormatted(vehicles);
        System.out.println('\n' + "Looking for something specific? " + '\n');
        System.out.println("(1) Filter cars only");
        System.out.println("(2) Filter planes only");
        System.out.println("(3) Filter by brand");
        System.out.println("(4) Sort by release year (DESC)");
        System.out.println("(5) Sort by price (ASC)");
        System.out.println("(0) I'm done displaying! ");
    }

    /**
     * Draws the menu for the editing.
     */
    public static void drawEditVehicleMenu(List<Vehicle> vehicles) {
        flushScreen();
        drawMenuHeaderLong();
        System.out.println("Here is a list of all your vehicles.");
        vehicleListPrinterFormatted(vehicles);
    }

    /**
     * Draws the menu for adding.
     */
    public static void drawAddVehicleMenu() {
        Utilities.drawMenuHeader();
        System.out.println('\n' + "What do you wanna add?" + '\n');
        System.out.println("(1) Car");
        System.out.println("(2) Plane");
        System.out.println("(0) I'm done adding... ");
    }

    /**
     * Draws the menu for the ordering.
     */
    public static void drawOrderMenu(List<Vehicle> vehicles) {
        Utilities.drawMenuHeaderLong();
        Utilities.vehicleListPrinterFormatted(vehicles);
    }

    /**
     * Prints a list of vehicles in a vertical way.
     *
     * @param vehicles the list of vehicles to be printed
     */
    public static void vehicleListPrinter(List<Vehicle> vehicles) {
        for (Vehicle v : vehicles) {
            System.out.println(v);
        }
    }

    /**
     * Prints a list of vehicles in a formatted table.
     *
     * @param vehicles the list of vehicles to be printed
     */
    public static void vehicleListPrinterFormatted(List<Vehicle> vehicles) {
        System.out.println(
                "ID BRAND      MODEL           YEAR  PRICE      COLOR      HORSEPOWER  PASSENGERS  MAX CARGO  MAX ALTITUDE    FUEL TYPE");
        System.out.println(
                "----------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < vehicles.size(); i++) {
            Vehicle v = vehicles.get(i);

            if (v instanceof Car) {
                System.out.println(
                        String.format("%-2s %-10s %-15s %-5s %-10s %-10s %-11s %-11s %-10s %-15s %-15s", i + 1,
                                v.getBrand(),
                                v.getModel(), v.getYear(),
                                v.getPrice(), v.getColor(), v.getHorsePower(), ((Car) v).getDoorCount(),
                                ((Car) v).getTrunkCapacity(), "N/A", ((Car) v).getFuelType()));

            } else if (v instanceof Plane) {
                System.out
                        .println(String.format("%-2s %-10s %-15s %-5s %-10s %-10s %-11s %-11s %-10s %-15s %-15s", i + 1,
                                v.getBrand(), v.getModel(), v.getYear(), v.getPrice(), v.getColor(), v.getHorsePower(),
                                ((Plane) v).getMaxSeatingCapacity(),
                                ((Plane) v).getMaxCargoCapacity(), ((Plane) v).getMaxAltitude(), "N/A"));

            }
        }
    }

    /**
     * Calculates the discount based on the loyalty program and purchase history.
     *
     * @param invSys  the InventorySystem instance
     * @param vehicle the vehicle for which the discount is calculated
     * @param email   the email of the customer
     * @return the calculated discount factor
     */
    public static double calculateDiscount(InventorySystem invSys, Vehicle vehicle, String email) {
        String purchaseBrand = vehicle.getBrand();
        if (!(checkIfRealCustomer(invSys, email))) {
            return 1;
        }

        int counter = 0;
        for (String brand : invSys.getLoyaltyMembers().get(email)) {
            if (brand.equals(purchaseBrand)) {
                counter++;
            }
        }

        return purchaseCountToDiscount(counter);
    }

    /**
     * Checks if a customer is part of the loyalty program.
     *
     * @param invSys the InventorySystem instance
     * @param email  the email of the customer
     * @return true if the customer is part of the loyalty program, false otherwise
     */
    public static boolean checkIfRealCustomer(InventorySystem invSys, String email) {
        if (invSys.getLoyaltyMembers().containsKey(email))
            return true;

        return false;
    }

    /**
     * Converts the purchase count to a discount multiplier.
     *
     * @param purchaseCount the number of previous purchases
     * @return the discount
     */
    public static double purchaseCountToDiscount(int purchaseCount) {
        if (purchaseCount <= 0)
            return 1;
        switch (purchaseCount) {
            case 1:
                return 0.95;
            case 2:
                return 0.90;
            default: // Cases where it's 3 or more...
                return 0.85;
        }
    }

}
