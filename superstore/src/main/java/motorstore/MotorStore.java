package motorstore;

import motorstore.dataImport.importer.ImportDataException;
import motorstore.guilayer.*;

public class MotorStore {
    public static void main(String[] args) throws ImportDataException {
        MainApp.runApplication();
    }
}
