DROP TABLE Vehicles;
DROP PACKAGE motorstore_package;
DROP TYPE CARTYP;
DROP TYPE PLATYP;

CREATE TABLE Vehicles (
    vehicleId           NUMBER(1)       GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
    type                VARCHAR2(5)     NOT NULL,
    brand               VARCHAR(30)     NOT NULL,
    model               VARCHAR(30)     NOT NULL,
    year                NUMBER(4)       NOT NULL,
    price               NUMBER(11,2)    NOT NULL,
    color               VARCHAR2(20)    NOT NULL,
    horsePower          NUMBER(6)       NOT NULL,
    doorCount           NUMBER(1),
    fuelType            VARCHAR2(20),
    trunkCapacity       NUMBER(5,2),
    maxAltitude         NUMBER(6),
    maxCargoCapacity    NUMBER(5,2),
    maxSeatingCapacity  NUMBER(4)
);
/

CREATE OR REPLACE TYPE CARTYP AS OBJECT
(
    brand               VARCHAR(30),
    model               VARCHAR(30),
    year                NUMBER(4),
    price               NUMBER(11,2),
    color               VARCHAR2(20),
    horsePower          NUMBER(6),
    doorCount           NUMBER(1),
    fuelType            VARCHAR2(20),
    trunkCapacity       NUMBER(5,2)
);
/

CREATE OR REPLACE TYPE PLATYP AS OBJECT
(
    brand               VARCHAR(30),
    model               VARCHAR(30),
    year                NUMBER(4),
    price               NUMBER(11,2),
    color               VARCHAR2(20),
    horsePower          NUMBER(6),
    maxAltitude         NUMBER(6),
    maxCargoCapacity    NUMBER(5,2),
    maxSeatingCapacity  NUMBER(4)
);
/

CREATE OR REPLACE PACKAGE motorstore_package AS
    PROCEDURE insert_Plane(typeRef Vehicles.type%TYPE, vRef PLATYP);
    PROCEDURE insert_Car(typeRef Vehicles.type%TYPE, vRef CARTYP);
    PROCEDURE load_vehicles(vehicles OUT SYS_REFCURSOR, typeRef OUT SYS_REFCURSOR);
END motorstore_package;
/

CREATE OR REPLACE PACKAGE BODY motorstore_package AS
    PROCEDURE insert_Plane(typeRef Vehicles.type%TYPE, vRef PLATYP) AS
    BEGIN
        INSERT INTO Vehicles (type, brand, model, year, price, color, horsePower, doorCount, fuelType, trunkCapacity, maxAltitude, maxCargoCapacity, maxSeatingCapacity)
        VALUES (typeRef, vRef.brand, vRef.model, vRef.year, vRef.price, vRef.color, vRef.horsePower, null, null, null, vRef.maxAltitude, vRef.maxCargoCapacity, vRef.maxSeatingCapacity);
    END insert_Plane;
    
    PROCEDURE insert_Car(typeRef Vehicles.type%TYPE, vRef CARTYP) AS
    BEGIN
        INSERT INTO Vehicles (type, brand, model, year, price, color, horsePower, doorCount, fuelType, trunkCapacity, maxAltitude, maxCargoCapacity, maxSeatingCapacity)
        VALUES (typeRef, vRef.brand, vRef.model, vRef.year, vRef.price, vRef.color, vRef.horsePower, vRef.doorCount, vRef.fuelType, vRef.trunkCapacity, null, null, null);
    EXCEPTION
        WHEN OTHERS THEN
            raise_application_error(-20000, 'Unknown error');
    END insert_Car;
    
    PROCEDURE load_vehicles(vehicles OUT SYS_REFCURSOR, typeRef OUT SYS_REFCURSOR) AS
    BEGIN
        OPEN vehicles FOR
            SELECT  brand AS "brand",
                    model AS "model",
                    year AS "year",
                    price AS "price",
                    color AS "color",
                    horsePower AS "horsePower",
                    doorCount AS "doorCount",
                    fuelType AS "fuelType",
                    trunkCapacity AS "trunkCapacity",
                    maxAltitude AS "maxAltitude",
                    maxCargoCapacity AS "maxCargoCapacity",
                    maxSeatingCapacity AS "maxSeatingCapacity"
            FROM Vehicles;
        OPEN typeRef FOR
        SELECT
            type AS "type"
        FROM
            Vehicles;
    END load_vehicles;
END motorstore_package;
/