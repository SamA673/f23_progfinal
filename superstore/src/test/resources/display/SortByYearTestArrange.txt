Car,Chevrolet,Equinox,2023,31000.30,Blue,170,4,Gasoline,29
Car,Nissan,Rogue,2022,27000.60,Red,180,4,Gasoline,32
Car,Toyota,Camry,2022,28000.75,Silver,203,4,Gasoline,16
Car,Ford,Escape,2021,29000.90,Gray,250,4,Gasoline,34
Car,Honda,CR-V,2020,32000.60,Black,190,4,Gasoline,39
Plane,Boeing,777,2019,320000000.00,White,43000,28000,368,300
Plane,Cessna,Citation X+,2019,23000000.00,Red,20000,15000,50,12
Plane,Boeing,737,2018,850000,White,44000,35000,230,400
Plane,Embraer,E175,2016,55000000.00,Green,33000,22000,88,5
Plane,Airbus,A320,2015,95000000.00,Blue,48000,29000,150,300