package motorstore;

import org.junit.Test;

import motorstore.dataImport.importer.*;
import motorstore.datalogic.InventorySystem;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.display.SortStrategy;
import motorstore.datalogic.display.sortComparators.PriceComparator;
import motorstore.datalogic.product.*;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class InventorySystemTest {
    @Test
    public void testGetMethods() {
        Car car = new Car("Nissan", "Rogue", 2022, 27000.60, "Red", 180, 4, "Gasoline", 32);
        Plane plane = new Plane("Boeing", "737", 2018, 85000000.00, "White", 44000, 35000, 230, 400);
        List<Vehicle> vehicles = new ArrayList<Vehicle>();
        vehicles.add(car);
        vehicles.add(plane);
        IVehicleImporter importer = new FileJavaImporter("src/test/resources/testFile.txt");
        try {
            InventorySystem invSys = new InventorySystem(importer);
            invSys.setVehicleDisplayer(new SortStrategy(invSys.getVehicles(), new PriceComparator()));
            // Test if types are of correct object
            assertTrue(invSys.getVehicles().get(0) instanceof Car);
            assertTrue(invSys.getVehicles().get(1) instanceof Plane);
            // Test size of returned list
            assertEquals(10, invSys.getVehicles().size());
            // Test if it returns a displayer
            assertTrue(invSys.getVehicleDisplayer() instanceof IVehicleDisplayer);
        } catch (ImportDataException e) {
            fail("ImportDataException should not be thrown");
        }
    }

    @Test
    public void testEdit() {
        try {
            Car car = new Car("Kachow", "Rogue", 2022, 27000.60, "Red", 180, 4, "Gasoline", 32);
            IVehicleImporter importer = new FileJavaImporter("src/test/resources/testFile.txt");
            InventorySystem invSys = new InventorySystem(importer);
            invSys.editVehicle(1, car);
            assertEquals("Kachow", invSys.getVehicles().get(1).getBrand());
        } catch (ImportDataException e) {
            fail("IO Exception occured");
        }
    }

    @Test
    public void testAdd() {
        try {
            Car car = new Car("Kachow", "Rogue", 2022, 27000.60, "Red", 180, 4, "Gasoline", 32);
            IVehicleImporter importer = new FileJavaImporter("src/test/resources/testFile.txt");
            InventorySystem invSys = new InventorySystem(importer);
            invSys.addVehicle(car);

            assertEquals(invSys.getVehicles().get(10).getBrand(), car.getBrand());
            assertEquals(invSys.getVehicles().get(10).getModel(), car.getModel());
            assertEquals(invSys.getVehicles().get(10).getYear(), car.getYear());
            assertEquals(invSys.getVehicles().get(10).getPrice(), car.getPrice(), 0);
            assertEquals(invSys.getVehicles().get(10).getColor(), car.getColor());
            assertEquals(invSys.getVehicles().get(10).getHorsePower(), car.getHorsePower());

        } catch (ImportDataException e) {
            fail("IO Exception occured");
        }

    }

}
