package motorstore.importer;

import org.junit.Test;
import static org.junit.Assert.*;

import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.dataImport.importer.ImportDataException;
import motorstore.datalogic.product.*;

import java.util.List;

public class FileJavaImporterTest {
    @Test
    public void testLoadVehicles() {
        String fileName = "src/test/resources/testFile.txt";
        FileJavaImporter importer = new FileJavaImporter(fileName);
        try {
            List<Vehicle> vehicles = importer.loadVehicles();
            // Tests if loadVehicles has correct number of entries
            assertEquals(10, vehicles.size());
            // Tests if loadVehicles has correct object types
            assertTrue(vehicles.get(0) instanceof Car);
            assertTrue(vehicles.get(1) instanceof Plane);
        } catch (ImportDataException e){
            fail("Exception should not happen: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
