package motorstore.product;

import org.junit.Test;

import motorstore.datalogic.product.Plane;

import static org.junit.Assert.assertEquals;

public class PlaneTest {
    
    @Test
    public void testConstructorAndGetters() {
        Plane plane = new Plane("Boeing", "737", 2018, 85000000.87, "White", 44000, 35000, 230, 400);
        assertEquals("Boeing", plane.getBrand());
        assertEquals("737", plane.getModel());
        assertEquals(2018, plane.getYear());
        assertEquals(85000000.87, plane.getPrice(), 0);
        assertEquals("White", plane.getColor());
        assertEquals(44000, plane.getHorsePower());
        assertEquals(35000, plane.getMaxAltitude());
        assertEquals(230, plane.getMaxCargoCapacity(), 0);
        assertEquals(400, plane.getMaxSeatingCapacity(), 0);
    }

    @Test
    public void testStringArrayConstructor() {
        String[] fields = {"Plane", "Boeing", "737", "2018", "85000000.87", "White", "44000", "35000", "230", "400"};
        Plane plane = new Plane(fields);
        assertEquals("Boeing", plane.getBrand());
        assertEquals("737", plane.getModel());
        assertEquals(2018, plane.getYear());
        assertEquals(85000000.87, plane.getPrice(), 0);
        assertEquals("White", plane.getColor());
        assertEquals(44000, plane.getHorsePower());
        assertEquals(35000, plane.getMaxAltitude());
        assertEquals(230, plane.getMaxCargoCapacity(), 0);
        assertEquals(400, plane.getMaxSeatingCapacity(), 0);
    }

    @Test
    public void testToStringPlane() {
        Plane plane = new Plane("Boeing", "737", 2018, 85000000.87, "White", 44000, 35000, 230, 400);
        String result = plane.toString();
        assertEquals("Boeing 737 (Plane)\nYear: 2018\nPrice (USD): 85000000.87\nColor: White\nHorsepower: 44000\nMax Altitude (ft): 35000\nMax Cargo Capacity (Tons): 230.0\nMax Seating Capacity: 400\n", result);
    }
}
