package motorstore.product;

import org.junit.Test;

import motorstore.datalogic.product.Car;

import static org.junit.Assert.assertEquals;

public class CarTest {

    @Test
    public void testConstructorAndGetters() {
        Car car = new Car("Toyota", "Corolla", 2023, 16000.65, "red", 186, 4, "gasoline", 400.73);
        assertEquals("Toyota", car.getBrand());
        assertEquals("Corolla", car.getModel());
        assertEquals(2023, car.getYear());
        assertEquals(16000.65, car.getPrice(), 0);
        assertEquals("red", car.getColor());
        assertEquals(186, car.getHorsePower());
        assertEquals(4, car.getDoorCount());
        assertEquals("gasoline", car.getFuelType());
        assertEquals(400.73, car.getTrunkCapacity(), 0);
    }

    @Test
    public void testStringArrayConstructor() {
        String[] fields = {"Car", "Nissan", "Rogue", "2022", "27000.60", "Red", "180", "4", "Gasoline", "32"};
        Car car = new Car(fields);
        assertEquals("Nissan", car.getBrand());
        assertEquals("Rogue", car.getModel());
        assertEquals(2022, car.getYear());
        assertEquals(27000.60, car.getPrice(), 0);
        assertEquals("Red", car.getColor());
        assertEquals(180, car.getHorsePower());
        assertEquals(4, car.getDoorCount());
        assertEquals("Gasoline", car.getFuelType());
        assertEquals(32, car.getTrunkCapacity(), 0);
    }

    @Test
    public void testToStringCar() {
        Car car = new Car("Toyota", "Corolla", 2023, 16000.65, "red", 186, 4, "gasoline", 400.73);
        String result = car.toString();
        assertEquals("Toyota Corolla (Car)\nYear: 2023\nPrice (USD): 16000.65\nColor: red\nHorsepower: 186\nDoor count: 4\nFuel Type: gasoline\nTrunk Capacity (L): 400.73\n", result);
    }
}
