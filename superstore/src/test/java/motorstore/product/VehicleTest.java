package motorstore.product;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import motorstore.datalogic.product.Car;
import motorstore.datalogic.product.Vehicle;

public class VehicleTest {
    
    @Test
    public void testVehicleConstructorAndGetters() {
        Vehicle vehicle = new Car("Toyota", "Corolla", 2023, 16000.65, "red", 186, 4, "gasoline", 400.73);
        assertEquals("Toyota", vehicle.getBrand());
        assertEquals("Corolla", vehicle.getModel());
        assertEquals(2023, vehicle.getYear());
        assertEquals(16000.65, vehicle.getPrice(), 0);
        assertEquals("red", vehicle.getColor());
        assertEquals(186, vehicle.getHorsePower());
    }
}
