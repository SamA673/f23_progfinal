package motorstore.display;

import static org.junit.Assert.*;
import org.junit.Test;

import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.dataImport.importer.ImportDataException;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.display.filterStrategies.FilterBrand;
import motorstore.datalogic.product.Vehicle;

import java.util.List;

public class FilterBrandTest {
    @Test
    public void testArrange_ReturnsFilteredList() {
        FileJavaImporter actualImporter = new FileJavaImporter("src/test/resources/testFile.txt");
        try {
            List<Vehicle> actualVehicles = actualImporter.loadVehicles();
            IVehicleDisplayer displayer = new FilterBrand(actualVehicles, "Boeing");
            List<Vehicle> actualFilteredVehicles = displayer.arrange();
            for (Vehicle vehicle : actualFilteredVehicles) {
                assertEquals("Boeing", vehicle.getBrand());
            }
        } catch (ImportDataException e) {
            fail("Unexpected IOException occurred");
        }
    }
}
