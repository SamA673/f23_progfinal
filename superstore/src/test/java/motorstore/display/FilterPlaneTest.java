package motorstore.display;

import static org.junit.Assert.*;
import org.junit.Test;

import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.dataImport.importer.ImportDataException;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.display.filterStrategies.FilterPlane;
import motorstore.datalogic.product.*;

import java.util.List;

public class FilterPlaneTest {
    @Test
    public void testArrange_ReturnsFilteredList() {
        FileJavaImporter actualImporter = new FileJavaImporter("src/test/resources/testFile.txt");
        try {
            List<Vehicle> actualVehicles = actualImporter.loadVehicles();
            FileJavaImporter expectedimporter = new FileJavaImporter("src/test/resources/display/filterPlaneTestArrange.txt");
            List<Vehicle> expectedVehicles = expectedimporter.loadVehicles();
            IVehicleDisplayer displayer = new FilterPlane(actualVehicles);
            List<Vehicle> actualFilteredVehicles = displayer.arrange();
            for (int i = 0; i < actualFilteredVehicles.size() ; i++) {
                assertEquals(expectedVehicles.get(i).getBrand(), actualFilteredVehicles.get(i).getBrand());
                assertEquals(expectedVehicles.get(i).getModel(), actualFilteredVehicles.get(i).getModel());
                assertEquals(expectedVehicles.get(i).getYear(), actualFilteredVehicles.get(i).getYear());
                assertEquals(expectedVehicles.get(i).getPrice(), actualFilteredVehicles.get(i).getPrice(), 0);
                assertEquals(expectedVehicles.get(i).getColor(), actualFilteredVehicles.get(i).getColor());
                assertEquals(expectedVehicles.get(i).getHorsePower(), actualFilteredVehicles.get(i).getHorsePower());
                assertEquals(((Plane) expectedVehicles.get(i)).getMaxAltitude(), ((Plane) actualFilteredVehicles.get(i)).getMaxAltitude());
                assertEquals(((Plane) expectedVehicles.get(i)).getMaxCargoCapacity(), ((Plane) actualFilteredVehicles.get(i)).getMaxCargoCapacity(), 0);
                assertEquals(((Plane) expectedVehicles.get(i)).getMaxSeatingCapacity(), ((Plane) actualFilteredVehicles.get(i)).getMaxSeatingCapacity());
            }
        } catch (ImportDataException e) {
            fail("Unexpected IOException occurred");
        }
    }
}
