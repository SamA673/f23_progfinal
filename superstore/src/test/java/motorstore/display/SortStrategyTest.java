package motorstore.display;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import motorstore.dataImport.importer.FileJavaImporter;
import motorstore.dataImport.importer.ImportDataException;
import motorstore.datalogic.display.IVehicleDisplayer;
import motorstore.datalogic.display.SortStrategy;
import motorstore.datalogic.display.sortComparators.PriceComparator;
import motorstore.datalogic.display.sortComparators.YearComparator;
import motorstore.datalogic.product.Vehicle;

public class SortStrategyTest {
    @Test
    public void testArrange_ReturnsSortedByYearList() {
        FileJavaImporter actualImporter = new FileJavaImporter("src/test/resources/testFile.txt");
        try {
            List<Vehicle> actualVehicles = actualImporter.loadVehicles();
            FileJavaImporter expectedimporter = new FileJavaImporter(
                    "src/test/resources/display/SortByYearTestArrange.txt");

            List<Vehicle> expectedVehicles = expectedimporter.loadVehicles();
            IVehicleDisplayer displayer = new SortStrategy(actualVehicles, new YearComparator());

            List<Vehicle> actualSortedVehicles = displayer.arrange();
            for (int i = 0; i < actualSortedVehicles.size(); i++) {
                assertEquals(expectedVehicles.get(i).getBrand(), actualSortedVehicles.get(i).getBrand());
                assertEquals(expectedVehicles.get(i).getModel(), actualSortedVehicles.get(i).getModel());
                assertEquals(expectedVehicles.get(i).getYear(), actualSortedVehicles.get(i).getYear());
                assertEquals(expectedVehicles.get(i).getPrice(), actualSortedVehicles.get(i).getPrice(), 0);
                assertEquals(expectedVehicles.get(i).getColor(), actualSortedVehicles.get(i).getColor());
                assertEquals(expectedVehicles.get(i).getHorsePower(), actualSortedVehicles.get(i).getHorsePower());
            }
        } catch (ImportDataException e) {
            fail("Unexpected IOException occurred");
        }
    }

    @Test
    public void testArrange_ReturnsSortedByPriceList() {
        FileJavaImporter actualImporter = new FileJavaImporter("src/test/resources/testFile.txt");
        try {
            List<Vehicle> actualVehicles = actualImporter.loadVehicles();
            FileJavaImporter expectedimporter = new FileJavaImporter(
                    "src/test/resources/display/SortByPriceTestArrange.txt");

            List<Vehicle> expectedVehicles = expectedimporter.loadVehicles();
            IVehicleDisplayer displayer = new SortStrategy(actualVehicles, new PriceComparator());

            List<Vehicle> actualSortedVehicles = displayer.arrange();
            for (int i = 0; i < actualSortedVehicles.size(); i++) {
                assertEquals(expectedVehicles.get(i).getBrand(), actualSortedVehicles.get(i).getBrand());
                assertEquals(expectedVehicles.get(i).getModel(), actualSortedVehicles.get(i).getModel());
                assertEquals(expectedVehicles.get(i).getYear(), actualSortedVehicles.get(i).getYear());
                assertEquals(expectedVehicles.get(i).getPrice(), actualSortedVehicles.get(i).getPrice(), 0);
                assertEquals(expectedVehicles.get(i).getColor(), actualSortedVehicles.get(i).getColor());
                assertEquals(expectedVehicles.get(i).getHorsePower(), actualSortedVehicles.get(i).getHorsePower());
            }
        } catch (ImportDataException e) {
            fail("Unexpected IOException occurred");
        }
    }

}
