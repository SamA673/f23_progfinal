# MotorStore
MotorStore is an inventory management system for a store that sells cars and planes! 

## Setup
1. Download and extract or clone the following repo link...

```bash
https://gitlab.com/SamA673/f23_progfinal.git
```

2. Open the PROJECT FOLDER (f23_progfinal), not a subfolder(like superstore). For example, if you are in Visual Studio Code, your opened folder should look like this...
![alt text](https://cdn.discordapp.com/attachments/1139010005903888467/1180346613907062815/image.png?ex=657d1672&is=656aa172&hm=68604d8b6407354755a09801ba5ceec2c0adc3347601919705ef4c822ca6ebf8&)
This will ensure the relative paths work as intended! 


* PS. If you are using an SQL database for your products, make sure you run the setup package ("motorestore.sql") so that your Java Application can properly interact with the database. (Check the FAQ for further details on how to run MotorStore using an SQL connection!


## Launch
Launching is fairly straight forward! All you need to do is run: 

```bash
MotorStore.java
```

## Usage 
Once you are in the application, you will be prompted on if you are an admin or not. 


```console
<------------ Welcome to MotorStore ------------>
                (Version 2.0)                    

<SYSTEM> Are you an admin? ('y' if yes, 'n' if no!)
 
```

Press y if you are an admin or n otherwise and hit enter.
If you are an admin, you will be asked for a password. (By default, this is 123ILOVECARS)


### Options
MotorStore has a varying range of functionalities! 
```bash
<------------ Welcome to MotorStore ------------>
                (Version 2.0)                    

Hello Admin!
What would you like to do...?
(1) Create an order!
(2) Display
(3) Add Vehicle
(4) Edit Vehicle
(5) Delete Vehicle
(6) Save
(0) Exit!
<SYSTEM> Please enter a value.
```

*PS. Admins can do anything that a regular employee can and more, while employees have a restricted version of this menu (IE: Employees can not add,edit or delete vehicles)

#### (1) Create an order! 
This is where orders are inputted.
You will be presented with the full list of vehicles, along with an index value to the left. 

```
<----------------------------------------------- Welcome to MotorStore ----------------------------------------------->
                                                    (Version 2.0)                    

ID BRAND      MODEL           YEAR  PRICE      COLOR      HORSEPOWER  PASSENGERS  MAX CARGO  MAX ALTITUDE    FUEL TYPE
----------------------------------------------------------------------------------------------------------------------
1  Boeing     737             2018  850000.0   White      44000       400         230.0      35000           N/A            
2  Toyota     Camry           2022  28000.75   Silver     203         4           16.0       N/A             Gasoline       
3  Airbus     A320            2015  9.5E7      Blue       48000       300         150.0      29000           N/A            
4  Honda      CR-V            2020  32000.6    Black      190         4           39.0       N/A             Gasoline       
5  Cessna     Citation X+     2019  2.3E7      Red        20000       12          50.0       15000           N/A            
6  Ford       Escape          2021  29000.9    Gray       250         4           34.0       N/A             Gasoline       
7  Embraer    E175            2016  5.5E7      Green      33000       5           88.0       22000           N/A            
8  Chevrolet  Equinox         2023  31000.3    Blue       170         4           29.0       N/A             Gasoline       
9  Boeing     777             2019  3.2E8      White      43000       300         368.0      28000           N/A            
10 Nissan     Rogue           2022  27000.6    Red        180         4           32.0       N/A             Gasoline       
What would you like to buy? (Enter number associated with listing)
```

Press the index of the car the customer wants to buy and hit enter.
Then, enter the email of the customer.

##### The Loyalty System
The loyalty system may seem confusing at first glance, but it's actually fairly straight forward. Each customer has an email and will be asked to provide it during purchase after picking a vehicle.

If a given email matches an email found in our loyalty program, their previous orders will be checked. If the customer has previously bought a vehicle from the same brand as they are currently buying, they will get a 5% discount on their new vehicle. 

Furthermore , this discount is multiplied up to 3x, which means if a customer has bought 3 Hondas (regardless of model), they will get a 15% discount on their 4th, 5th, 6th Hondas. 


#### (2) Display
Displaying is easy, you will be presented with a list of all the vehicles and you can sort or filter them. Once you're done, press 0 to return to the menu. 

#### (3) Edit Vehicle
Editing is also easy, just enter the index of the vehicle and then answer the prompts to fill the values of all the new fields. 

#### (4) Delete Vehicle
A list will of all the vehicles will be presented to you and you will pick the index corresponding to the vehicle you'd like to remove. 

#### (5) Save
Save will log any new loyalty members (and the brands they have purchases) and/or changes made to the vehicle list itself.

*PS. Save only tracks product changes if you are running the SQL version of the app.*

#### (0) Exit
This is pretty straightforward...

## FAQ

### I just installed MotorStore and it seems to be asking me to input a password, what is it ? 

```bash
<------------ Welcome to MotorStore ------------>
                (Version 2.0)                    

<SYSTEM> Please enter the admin password
123ILOVECARS

```


*Shhh... Don't tell anyone we told you this.*

***

### I don't like the default password. I wanna change it! 

Sure! Open the project folder and navigate your way into AdminApp via the following path...


```
superstore/src/java/motorstore/guilayer/AdminApp.java 
```

Then, simply edit the 17th line so that it shows what you want it to. 

IE:

```console
public class AdminApp extends AppUI {
    private static final String adminPW = "MYNEWPASSWORD";
```

*PS. You will be able to change your password in release Version 3.0 (Expected Release Q2 2026)*
***

### I'd like to use my Oracle Server to track products, how do I do that?
Sure, you can do that! Navigate your way into MainApp.java via the following path:


```
superstore/src/java/motorstore/guilayer/MainApp.java 
```

Then, go to line 32 and change the following code...
```java
IVehicleImporter importer = new FileJavaImporter("superstore/src/main/resources/file.txt",
"superstore/src/main/resources/loyalty.txt");
```

So that it becomes...
```java
IVehicleImporter importer = new SqlVehicleImporter();

```

Then, launch the program as usual. 


*PS. We are aware that this is not optimal or intuitive and will be addressing it in Version 3.0!*



